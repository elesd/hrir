#include "image_reconstruct.h"

#include <stdlib.h>
#include <image_amplifier.h>
#include <haar_transformation.h>
#include <non_maxima.h>
#include <dumper.h>
#include <string.h>
#include <stdio.h>

#define PREWITT_ROW_MASK {-0.33, 0.0, 0.33,\
                          -0.33, 0.0, 0.33,\
                          -0.33, 0.0, 0.33}

#define PREWITT_COL_MASK {0.33, 0.33, 0.33,\
                          0.00, 0.00, 0.00,\
                          -0.33, -0.33, -0.33}

#define TRACE_IMAGE(data, width, height) { \
  char TRACE_IMAGE_tmp[128];snprintf(TRACE_IMAGE_tmp, 128, "%s_%d_%d", #data, width, height);\
  dumper_trace_to_file(TRACE_IMAGE_tmp, data, width * height);}


static image_reconstruct_ERROR_CODE
prepare_image(const image_filter_IMG* original,
              const image_reconstruct_PARAMETERS* params,
              image_filter_IMG* out);

/******************************************************************************/

static image_reconstruct_ERROR_CODE
apply_filter(const image_filter_IMG* original,
             image_filter_FILTER* filter,
             image_filter_IMG* out);

/******************************************************************************/

static void
calculate_sum(const image_filter_IMG* a,
              const image_filter_IMG* b,
              image_filter_IMG* out);

/******************************************************************************/

image_reconstruct_ERROR_CODE
image_reconstruct_from_lower_image(const image_filter_IMG* original,
                                   const image_reconstruct_PARAMETERS* params,
                                   image_filter_IMG* result)
{
  image_reconstruct_ERROR_CODE ret = IMAGE_RECONSTRUCT_OK;
  image_filter_ERROR_CODE filter_error = IMAGE_FILTER_OK;
  image_filter_FILTER row_filter = IMAGE_FILTER_INIT_FILTER;
  image_filter_FILTER col_filter = IMAGE_FILTER_INIT_FILTER;
  image_filter_IMG approx_row = IMAGE_FILTER_INIT_IMAGE;
  image_filter_IMG approx_col = IMAGE_FILTER_INIT_IMAGE;
  image_filter_IMG approx_diag = IMAGE_FILTER_INIT_IMAGE;
  image_filter_IMG tmp = IMAGE_FILTER_INIT_IMAGE;
  double row_mask[] = PREWITT_ROW_MASK;
  double col_mask[] = PREWITT_COL_MASK;


  image_filter_create_filter_by_mask(3,
                                     row_mask,
                                     &row_filter);
  image_filter_create_filter_by_mask(3,
                                     col_mask,
                                     &col_filter);
  image_filter_create_image(original->width,
                            original->height,
                            &approx_row);
  image_filter_create_image(original->width,
                            original->height,
                            &tmp);
  image_filter_create_image(original->width,
                            original->height,
                            &approx_col);
  image_filter_create_image(original->width,
                            original->height,
                            &approx_diag);
  if ((ret = prepare_image(original, params, &tmp))
             != IMAGE_AMPLIFIER_OK)
  {}
  else if ((ret = apply_filter(&tmp,
                               &row_filter,
                               &approx_row)) != IMAGE_RECONSTRUCT_OK)
  {}
  else if ((ret = apply_filter(&tmp,
                               &col_filter,
                               &approx_col)) != IMAGE_RECONSTRUCT_OK)
  {}

  if (ret == IMAGE_RECONSTRUCT_OK)
  {
    haar_transformation_DATA_SOURCE data;

    calculate_sum(&approx_col,
                  &approx_row,
                  &approx_diag);
 
    data.width = original->width;
    data.height = original->height;
    data.original_image = result->content;
    data.avg_image = tmp.content;
    data.vertical_edges = approx_col.content;
    data.horizontal_edges = approx_row.content;
    data.diagonal_edges = approx_diag.content;

  //  TRACE_IMAGE(data.avg_image, data.width, data.height);
 //   TRACE_IMAGE(data.vertical_edges, data.width, data.height);
 //   TRACE_IMAGE(data.horizontal_edges, data.width, data.height);
 //   TRACE_IMAGE(data.diagonal_edges, data.width, data.height);
 //   TRACE_IMAGE(tmp.content, data.width, data.height);
    data.width *= 2;
    data.height *= 2;
    haar_transformation_inverse_transformation(data);
 //   TRACE_IMAGE(data.original_image, data.width, data.height);
  }

  image_filter_destroy_image(&approx_diag);
  image_filter_destroy_image(&tmp);
  image_filter_destroy_image(&approx_col);
  image_filter_destroy_image(&approx_row);
  image_filter_destroy_filter(&row_filter);
  image_filter_destroy_filter(&col_filter);

  return ret;
  //destroy
}

/******************************************************************************/

static image_reconstruct_ERROR_CODE
prepare_image(const image_filter_IMG* original,
              const image_reconstruct_PARAMETERS* params,
              image_filter_IMG* out)
{
  non_maxima_GRADIENT_MASK mask = NON_MAXIMA_PREWITT_GRADIENT_MASK;
  image_amplifier_OPTIONS opt;
  switch (params->type)
  {
    case IMAGE_RECONSTRUCT_GAUSSIAN:
      opt.params.gaussian.sigma = params->params.gaussian_sigma;
      opt.type = IMAGE_AMPLIFIER_GAUSSIAN;
      opt.method = IMAGE_AMPLIFIER_APPLY_FILTER;
      break;
    case IMAGE_RECONSTRUCT_MEDIAN:
      opt.type = IMAGE_AMPLIFIER_MEDIAN;
      opt.method = IMAGE_AMPLIFIER_APPLY_FILTER;
      break;
    case IMAGE_RECONSTRUCT_NON_MAXIMA:
      opt.method = IMAGE_AMPLIFIER_NON_MAXIMA;
      switch (params->params.non_maxima_type)
      {
        case IMAGE_RECONSTRUCT_SOBEL:
          memcpy(opt.params.non_maxima.mask,
                 mask,
                 sizeof(non_maxima_GRADIENT_MASK));
          break;
        case IMAGE_RECONSTRUCT_PREWIT:
          memcpy(opt.params.non_maxima.mask,
                 mask,
                 sizeof(non_maxima_GRADIENT_MASK));
          break;
        default:
          /* ERROR*/
          return IMAGE_RECONSTRUCT_INVALID_FILTER;
      }
      break;
    deault:
      return IMAGE_RECONSTRUCT_INVALID_FILTER;
  }
  opt.filter_size = params->size;
  switch (params->scaling)
  {
    case IMAGE_RECONSTRUCT_SCALE_UP:
      opt.scale = image_amplifier_scale;
      break;
    case IMAGE_RECONSTRUCT_SCALE_TO_ORIGIN:
      opt.scale = image_amplifier_scale_to_origin;
      break;
    case IMAGE_RECONSTRUCT_SCALE_AVG:
      opt.scale = image_amplifier_avg_scale;
      break;
    case IMAGE_RECONSTRUCT_SCALE_CUT:
      opt.scale = image_amplifier_cut_scale;
      break;
    default:
          /* ERROR*/
      return IMAGE_RECONSTRUCT_INVALID_SCALE;
  }

  if (image_amplifier_details_up(original,
                                 &opt,
                                 out) != IMAGE_AMPLIFIER_OK)
    return IMAGE_RECONSTRUCT_PREPARATION_ERROR;


  return IMAGE_AMPLIFIER_OK;
}

/******************************************************************************/

static image_reconstruct_ERROR_CODE
apply_filter(const image_filter_IMG* original,
             image_filter_FILTER* filter,
             image_filter_IMG* out)
{
  image_filter_ERROR_CODE filter_error;
  image_filter_FILTER_GRID result;
  result = malloc(sizeof(image_filter_MASK_DATA) * original->width * original->height);
  assert(result != NULL);

  filter_error = image_filter_apply_filter(original, filter, result);
  if (filter_error != IMAGE_FILTER_OK)
    return IMAGE_RECONSTRUCT_APPROX_ERROR;


  image_amplifier_scale_to_origin(result, original, out);
  free(result);
  return IMAGE_RECONSTRUCT_OK;
}

/******************************************************************************/

static void
calculate_sum(const image_filter_IMG* a,
              const image_filter_IMG* b,
              image_filter_IMG* out)
{
  image_filter_FILTER_GRID result;
  int i;
  result = malloc(sizeof(image_filter_MASK_DATA) * a->width * a->height);
  assert(result != NULL);
  
  for (i = 0; i < a->height * a->width; ++i)
    result[i] = a->content[i] + b->content[i];
  image_amplifier_scale_to_origin(result, a, out);
  free(result);
}  
