#ifndef NON_MAXIMA
#define NON_MAXIMA

#define NON_MAXIMA_WINDOW_SIZE 3

#define NON_MAXIMA_PREWITT_GRADIENT_MASK {-1, 0, 1,\
                                          -1, 0, 1,\
                                          -1, 0, 1}

#define NON_MAXIMA_SOBEL_GRADIENT_MASK {-1, 0, 1,\
                                        -2, 0, 2,\
                                        -1, 0, 1}
/*************************************************************************/
typedef unsigned char* non_maxima_DATA_GRID;

/*************************************************************************/
typedef int* non_maxima_GRAD_GRID;

/*************************************************************************/
typedef	int non_maxima_GRADIENT_MASK[NON_MAXIMA_WINDOW_SIZE * 3];

/************************* FUNCTIONS *************************************/
void
non_maxima_init_data_grid
(
  non_maxima_DATA_GRID* data,
	unsigned int size
);

/*************************************************************************/
void
non_maxima_init_grad_grid
(
  non_maxima_GRAD_GRID* data,
	unsigned int size
);

/*************************************************************************/
void
non_maxima_destroy_data_grid
(
  non_maxima_DATA_GRID data
);

/*************************************************************************/
void
non_maxima_destroy_grad_grid
(
  non_maxima_GRAD_GRID data
);

/*************************************************************************/
void
non_maxima_process_image
(
  const non_maxima_DATA_GRID input,
	const non_maxima_GRADIENT_MASK mask,
	const unsigned int size_x,
	const unsigned int size_y,
	non_maxima_DATA_GRID output
);

/*************************************************************************/
void
non_maxima_process_image_with_grad_map
(
  const non_maxima_DATA_GRID input,
	const non_maxima_GRADIENT_MASK mask,
	const unsigned int size_x,
	const unsigned int size_y,
	non_maxima_DATA_GRID output,
	non_maxima_GRAD_GRID grad_mag_map
);

#endif

