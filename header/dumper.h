#ifndef DUMPER_H
#define DUMPER_H

#define DUMPER_OK 0
#define DUMPER_FILE_OPEN_ERROR -1
#define DUMPER_WRITE_ERROR -2

/******************************************************************************/

typedef int dumper_ERROR_CODE;

/******************************************************************************/

dumper_ERROR_CODE
dumper_trace_to_file(const char* file_name,
                     const void* data, 
                     unsigned length);
                  

#endif // DUMPER_H
