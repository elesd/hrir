#ifndef PERCENT_CALCULATOR_H
#define PERCENT_CALCULATOR_H


typedef struct
{
	int min;
	int max;
} percent_calculator_CALCULATOR;
/**********************************************************************/
float
percent_calculator_calc
(
  const int act_value,
	const percent_calculator_CALCULATOR calc
);

#endif

