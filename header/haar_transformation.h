#ifndef HAAR_TRANSFORMATION_H
#define HAAR_TRANSFORMATION_H

/***********************************************************/
typedef unsigned char* haar_transformation_DATA_GRID;

/***********************************************************/
/**
  * @brief data source of the transformation
  */
typedef struct
{
  unsigned width;
  unsigned height;
  haar_transformation_DATA_GRID original_image;
  haar_transformation_DATA_GRID avg_image;
  haar_transformation_DATA_GRID vertical_edges;
  haar_transformation_DATA_GRID horizontal_edges;
  haar_transformation_DATA_GRID diagonal_edges;
} haar_transformation_DATA_SOURCE;

/***********************************************************/
/**
  * @brief Transformation
  * The all resoult is half times as the original
  * @param data_source The data source
  */
void
haar_transformation_transform
(
  haar_transformation_DATA_SOURCE data_source
);

/**
  * @brief Inverse transformation
  * The all resoult is half times as the original
  * @param data_source The data source
  */
void 
haar_transformation_inverse_transformation
(
  haar_transformation_DATA_SOURCE data_source
);

#endif

