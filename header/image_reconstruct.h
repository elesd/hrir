#ifndef IMAGE_RECONSTRUCT_H
#define IMAGE_RECONSTRUCT_H

#include "image_filter.h"

#define IMAGE_RECONSTRUCT_OK 0

#define IMAGE_RECONSTRUCT_APPROX_ERROR -8
#define IMAGE_RECONSTRUCT_INVALID_FILTER -9
#define IMAGE_RECONSTRUCT_INVALID_SCALE -10
#define IMAGE_RECONSTRUCT_PREPARATION_ERROR -10
#define IMAGE_RECONSTRUCT_MEM_ERROR -20

/******************************************************************************/

typedef int image_reconstruct_ERROR_CODE;

/******************************************************************************/

/** The enumeration of known edge detection during non maxima method.*/
typedef enum
{
  IMAGE_RECONSTRUCT_SOBEL,
  IMAGE_RECONSTRUCT_PREWIT
} image_reconstruct_NON_MAXIMA_FILTER_TYPE;

/******************************************************************************/

/** The enumeration of known edge scale method*/
typedef enum
{
  /** Scale the preprocessed image like the original */
  IMAGE_RECONSTRUCT_SCALE_TO_ORIGIN,
  /** Scale the preprocessed image to the whole boundary [0,255] */
  IMAGE_RECONSTRUCT_SCALE_UP,
  /** Cut the preprocessed image in the boundary */
  IMAGE_RECONSTRUCT_SCALE_CUT,
  /** Get the preprocessed and the original image avarage */
  IMAGE_RECONSTRUCT_SCALE_AVG
} image_reconstruct_SCALE_METHOD;

/******************************************************************************/

/**
* @brief the type of different preprocessor methods
*/
typedef enum
{
  /** In that case the edges will be increased by non maxima edge detection */
  IMAGE_RECONSTRUCT_NON_MAXIMA,
  /** In that case the edges will be increased by Median filter
  * @see #image_aplifier_details_up 
  */
  IMAGE_RECONSTRUCT_MEDIAN,

  /** In that case the edges will be increased by Gaussian filter
  * @see #image_aplifier_details_up 
  */
  IMAGE_RECONSTRUCT_GAUSSIAN
} image_reconstruct_PREPROCESS_TYPE;

/******************************************************************************/

/**
* @brief Parameter for image preprocessing
*/
typedef struct
{
  union
  {
    /** Gaussian filter param */
    double gaussian_sigma;
    /** non maxima filter type */
    image_reconstruct_NON_MAXIMA_FILTER_TYPE non_maxima_type;
  } params;

  /** The type of the preprocessing */
  image_reconstruct_PREPROCESS_TYPE type;

  /** The size of the applied filter */
  unsigned size;

  /** The way of the scaling method */
  image_reconstruct_SCALE_METHOD scaling;

} image_reconstruct_PARAMETERS;
 
/******************************************************************************/

/**
  * @brief Create a double sized image from the original
  * @details First step the code increase the intensity of the details with 
  *          the given mehtod. Thereafter calculate the horizontal and vertical
  *          edges. With these two images calculates the diagonal edges. Finally
  *          with an inverse Haar transformation calculates the bigger image.
  * @param original the original image
  * @param params the way of the intensity increasing
  * @param result Result image with double size
  * @return The error code
  */
image_reconstruct_ERROR_CODE
image_reconstruct_from_lower_image(const image_filter_IMG* original,
                                   const image_reconstruct_PARAMETERS* params,
                                   image_filter_IMG* result);

#endif
